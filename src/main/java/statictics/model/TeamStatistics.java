package statictics.model;

import com.google.gson.annotations.SerializedName;

public class TeamStatistics {
    private String country;

    @SerializedName("attempts_on_goal")
    private int attemptOnGoal;

    @SerializedName("on_target")
    private int onTarget;

    @SerializedName("off_target")
    private int offTarget;
    private int blocked;
    private int corners;
    private int offsides;

    @SerializedName("ball_possession")
    private int ballPossession;

    @SerializedName("pass_accuracy")
    private int passAccuracy;

    @SerializedName("num_passes")
    private int numPasses;

    @SerializedName("passes_completed")
    private int passesCompleted;

    @SerializedName("distance_covered")
    private int distanceCovered;
    private int tackles;
    private int clearances;

    @SerializedName("yellow_cards")
    private int yellowCards;

    @SerializedName("red_cards")
    private int redCards;

    @SerializedName("fouls_committed")
    private int foulsCommitted;
    private String tactics;

    public TeamStatistics() {
    }

    public TeamStatistics(String country, int attemptOnGoal, int onTarget, int offTarget,
                          int blocked, int corners, int offsides, int ballPossession, int passAccuracy,
                          int numPasses, int passesCompleted, int distanceCovered, int tackles, int clearances,
                          int yellowCards, int redCards, int foulsCommitted, String tactics) {
        this.country = country;
        this.attemptOnGoal = attemptOnGoal;
        this.onTarget = onTarget;
        this.offTarget = offTarget;
        this.blocked = blocked;
        this.corners = corners;
        this.offsides = offsides;
        this.ballPossession = ballPossession;
        this.passAccuracy = passAccuracy;
        this.numPasses = numPasses;
        this.passesCompleted = passesCompleted;
        this.distanceCovered = distanceCovered;
        this.tackles = tackles;
        this.clearances = clearances;
        this.yellowCards = yellowCards;
        this.redCards = redCards;
        this.foulsCommitted = foulsCommitted;
        this.tactics = tactics;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getAttemptOnGoal() {
        return attemptOnGoal;
    }

    public void setAttemptOnGoal(int attemptOnGoal) {
        this.attemptOnGoal = attemptOnGoal;
    }

    public int getOnTarget() {
        return onTarget;
    }

    public void setOnTarget(int onTarget) {
        this.onTarget = onTarget;
    }

    public int getOffTarget() {
        return offTarget;
    }

    public void setOffTarget(int offTarget) {
        this.offTarget = offTarget;
    }

    public int getBlocked() {
        return blocked;
    }

    public void setBlocked(int blocked) {
        this.blocked = blocked;
    }

    public int getCorners() {
        return corners;
    }

    public void setCorners(int corners) {
        this.corners = corners;
    }

    public int getOffsides() {
        return offsides;
    }

    public void setOffsides(int offsides) {
        this.offsides = offsides;
    }

    public int getBallPossession() {
        return ballPossession;
    }

    public void setBallPossession(int ballPossession) {
        this.ballPossession = ballPossession;
    }

    public int getPassAccuracy() {
        return passAccuracy;
    }

    public void setPassAccuracy(int passAccuracy) {
        this.passAccuracy = passAccuracy;
    }

    public int getNumPasses() {
        return numPasses;
    }

    public void setNumPasses(int numPasses) {
        this.numPasses = numPasses;
    }

    public int getPassesCompleted() {
        return passesCompleted;
    }

    public void setPassesCompleted(int passesCompleted) {
        this.passesCompleted = passesCompleted;
    }

    public int getDistanceCovered() {
        return distanceCovered;
    }

    public void setDistanceCovered(int distanceCovered) {
        this.distanceCovered = distanceCovered;
    }

    public int getTackles() {
        return tackles;
    }

    public void setTackles(int tackles) {
        this.tackles = tackles;
    }

    public int getClearances() {
        return clearances;
    }

    public void setClearances(int clearances) {
        this.clearances = clearances;
    }

    public int getYellowCards() {
        return yellowCards;
    }

    public void setYellowCards(int yellowCards) {
        this.yellowCards = yellowCards;
    }

    public int getRedCards() {
        return redCards;
    }

    public void setRedCards(int redCards) {
        this.redCards = redCards;
    }

    public int getFoulsCommitted() {
        return foulsCommitted;
    }

    public void setFoulsCommitted(int foulsCommitted) {
        this.foulsCommitted = foulsCommitted;
    }

    public String getTactics() {
        return tactics;
    }

    public void setTactics(String tactics) {
        this.tactics = tactics;
    }

    @Override
    public String toString() {
        return "TeamStatistics{" +
                "country=" + country +")";
    }
}


