package statictics.model;

import com.google.gson.annotations.SerializedName;

public class Weather {

    private int humidity;

    @SerializedName("temp_celsius")
    private int tempCelsius;

    @SerializedName("temp_farenheit")
    private int tempFarenheit;

    @SerializedName("wind_speed")
    private int windSpeed;

    private String description;

    public Weather() {
    }

    public Weather(int humidity, int tempCelsius, int tempFarenheit, int windSpeed, String description) {
        this.humidity = humidity;
        this.tempCelsius = tempCelsius;
        this.tempFarenheit = tempFarenheit;
        this.windSpeed = windSpeed;
        this.description = description;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public int getTempCelsius() {
        return tempCelsius;
    }

    public void setTempCelsius(int tempCelsius) {
        this.tempCelsius = tempCelsius;
    }

    public int getTempFarenheit() {
        return tempFarenheit;
    }

    public void setTempFarenheit(int tempFarenheit) {
        this.tempFarenheit = tempFarenheit;
    }

    public int getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(int windSpeed) {
        this.windSpeed = windSpeed;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Weather{" +
                "humidity=" + humidity +
                ", tempCelsius=" + tempCelsius +
                ", tempFarenheit=" + tempFarenheit +
                ", windSpeed=" + windSpeed +
                ", description='" + description + '\'' +
                '}';
    }
}
