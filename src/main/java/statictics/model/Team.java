package statictics.model;

public class Team {

    private String country;
    private String code;
    private int goals;
    private int penalties;

    public Team() {
    }

    public Team(String country, String code, int goals, int penalties) {
        this.country = country;
        this.code = code;
        this.goals = goals;
        this.penalties = penalties;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getGoals() {
        return goals;
    }

    public void setGoals(int goals) {
        this.goals = goals;
    }

    public int getPenalties() {
        return penalties;
    }

    public void setPenalties(int penalties) {
        this.penalties = penalties;
    }


}

