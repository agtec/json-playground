package statictics.model;

import statictics.network.HttpClient;

public class GetMatchesCommand extends BaseCommand<Match> {

    HttpClient httpClient;

    public GetMatchesCommand(HttpClient httpClient) {
        super(httpClient);
    }

    @Override
    String getUrl() {
        return "https://worldcup.sfg.io/matches";
    }

    @Override
    Class<Match[]> getClassToDeserialize() {
        return Match[].class;
    }
}
