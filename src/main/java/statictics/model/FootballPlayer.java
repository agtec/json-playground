package statictics.model;

import com.google.gson.annotations.SerializedName;

public class FootballPlayer {

    private String name;
    @SerializedName("capitan")
    private boolean isCapitan;

    @SerializedName("shirt_number")
    private int shirtNumber;
    private String position;

    public FootballPlayer() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCapitan() {
        return isCapitan;
    }

    public void setCapitan(boolean capitan) {
        isCapitan = capitan;
    }

    public int getShirtNumber() {
        return shirtNumber;
    }

    public void setShirtNumber(int shirtNumber) {
        this.shirtNumber = shirtNumber;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
