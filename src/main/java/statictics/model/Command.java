package statictics.model;

import java.io.IOException;
import java.util.List;

public interface Command<T> {

    List<T> fetch() throws IOException;
}