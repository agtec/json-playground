package statictics;

import statictics.model.Command;
import statictics.model.GetMatchesCommand;
import statictics.model.Match;
import statictics.network.HttpClient;

import java.io.IOException;
import java.util.stream.Stream;

public class App {

    public static void main(String[] args) throws IOException {
        HttpClient httpClient = new HttpClient();
        Command<Match> getMatchCommand =
                new GetMatchesCommand(httpClient);

        System.out.println("Teams whose players received a red card: ");

        getMatchCommand.fetch().stream()
                .flatMap((match -> {
                    return Stream.of(match.getHomeTeamStatistics(),
                            match.getAwayTeamStatistics());
                }))
                .filter(teamStatistics -> teamStatistics.getRedCards() > 0)
                .forEach(System.out::println);

        System.out.println("Teams whose players did not receive a single yellow and red card during the match: ");

        getMatchCommand.fetch().stream()
                .flatMap((match -> {
                    return Stream.of(match.getHomeTeamStatistics(),
                            match.getAwayTeamStatistics());
                }))
                .filter((teamStatistics -> teamStatistics.getRedCards() == 0
                        & teamStatistics.getYellowCards() == 0))
                .forEach(System.out::println);

        System.out.println("Teams that played in a 4-3-3 set up: ");

        getMatchCommand.fetch().stream()
                .flatMap((match -> {
                    return Stream.of(match.getHomeTeamStatistics(),
                            match.getAwayTeamStatistics());
                }))
                .filter((teamStatistics -> teamStatistics.getTactics().equals("4-3-3")))
                .forEach(System.out::println);

        System.out.println("Teams with ball possession of over 65%: ");
        getMatchCommand.fetch().stream()
                .flatMap((match -> {
                    return Stream.of(match.getHomeTeamStatistics(),
                            match.getAwayTeamStatistics());
                }))
                .filter(teamStatistics -> teamStatistics.getBallPossession() > 65)
                .forEach(System.out::println);
    }

}
